const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  // Update
  // const room = await Room.findById('62225d1930e62c2eac1131fd')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('----------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const buildings = await Building.find({}).populate('rooms')
  console.log(buildings)
}
main().then(() => {
  console.log('Finsih')
})
