const mongoose = require('mongoose')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')

async function main () {
  const newInformaticsBuilding = await Building.findById('62225d1930e62c2eac1131fc')
  const room = await Room.findById('62225d1a30e62c2eac113201')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}
main().then(() => {
  console.log('Finsih')
})
